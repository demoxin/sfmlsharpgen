﻿using ClangSharp;
using System;

namespace SFMLSharpGen
{
    internal interface ICXCursorVisitor
    {
        CXChildVisitResult Visit(CXCursor cursor, CXCursor parent, IntPtr data);
    }
}