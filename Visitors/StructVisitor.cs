﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClangSharp;

namespace SFMLSharpGen.Visitors
{
    public class StructVisitor : ICXCursorVisitor
    {
        CXChildVisitResult ICXCursorVisitor.Visit(CXCursor cursor, CXCursor parent, IntPtr data)
        {
            return CXChildVisitResult.CXChildVisit_Continue;
        }
    }
}
