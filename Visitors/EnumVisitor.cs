﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClangSharp;

namespace SFMLSharpGen.Visitors
{
    public class EnumVisitor : ICXCursorVisitor
    {
        public Dictionary<string, Int64> Values;

        public EnumVisitor()
        {
            Values = new Dictionary<string, long>();
        }

        public CXChildVisitResult Visit(CXCursor cursor, CXCursor parent, IntPtr data)
        {
            if (clang.getCursorKind(cursor) != CXCursorKind.CXCursor_EnumConstantDecl)
                return CXChildVisitResult.CXChildVisit_Continue;

            Values.Add(clang.getCursorSpelling(cursor).ToString(), clang.getEnumConstantDeclValue(cursor));

            return CXChildVisitResult.CXChildVisit_Continue;
        }
    }
}
