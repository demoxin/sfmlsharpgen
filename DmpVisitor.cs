﻿using System;
using System.Collections.Generic;
using System.Xml;
using ClangSharp;
using SFMLSharpGen.Data;
using SFMLSharpGen.Visitors;

namespace SFMLSharpGen
{
    internal class DmpVisitor : ICXCursorVisitor
    {
        NamedNode node;

        private static List<string> blacklist = new List<string>();

        static DmpVisitor()
        {
            blacklist.Add("priv");
            blacklist.Add("ThreadLocal");
        }

        public DmpVisitor(NamedNode xml)
        {
            this.node = xml;
        }

        public CXChildVisitResult Visit(CXCursor cursor, CXCursor parent, IntPtr data)
        {
            if (cursor.IsInSystemHeader())
            {
                return CXChildVisitResult.CXChildVisit_Continue;
            }

            NamedNode node;
            ICXCursorVisitor visitor = null;

            switch(clang.getCursorKind(cursor))
            {
                case CXCursorKind.CXCursor_Namespace:
                    if (blacklist.Contains(clang.getCursorSpelling(cursor).ToString()))
                        return CXChildVisitResult.CXChildVisit_Continue;
                    node = this.node.GetNode(clang.getCursorSpelling(cursor).ToString());
                    visitor = new DmpVisitor(node);
                    clang.visitChildren(cursor, visitor.Visit, new CXClientData(IntPtr.Zero));
                    break;
                case CXCursorKind.CXCursor_ClassDecl:
                    if (blacklist.Contains(clang.getCursorSpelling(cursor).ToString()))
                        return CXChildVisitResult.CXChildVisit_Continue;
                    node = new NamedNodeClass(clang.getCursorSpelling(cursor).ToString(), this.node);
                    visitor = new DmpVisitor(node);
                    clang.visitChildren(cursor, visitor.Visit, new CXClientData(IntPtr.Zero));
                    break;
                case CXCursorKind.CXCursor_FieldDecl:
                    if(clang.getCXXAccessSpecifier(cursor) != CX_CXXAccessSpecifier.CX_CXXPublic)
                        return CXChildVisitResult.CXChildVisit_Continue;
                    node = new NamedNodeField(clang.getCursorSpelling(cursor).ToString(), clang.getTypeSpelling(clang.getCursorType(cursor)).ToString(), false, this.node);
                    break;
                case CXCursorKind.CXCursor_CXXMethod:
                    if (clang.getCXXAccessSpecifier(cursor) != CX_CXXAccessSpecifier.CX_CXXPublic)
                        return CXChildVisitResult.CXChildVisit_Continue;
                    node = new NamedNodeMethod(clang.getCursorSpelling(cursor).ToString(), clang.getCursorResultType(cursor).ToString(), clang.CXXMethod_isStatic(cursor) == 1, null, this.node);
                    int paramCount = clang.Cursor_getNumArguments(cursor);
                    for(int i = 0; i < paramCount; i++)
                    {
                        String argType = clang.getTypeSpelling(clang.getArgType(clang.getCursorType(cursor), (uint)i)).ToString();
                        String argName = clang.getCursorSpelling(clang.Cursor_getArgument(cursor, (uint)i)).ToString();
                        ((NamedNodeMethod)node).AddParameter(argType, argName);
                    }
                    break;
                case CXCursorKind.CXCursor_VarDecl:
                    if (clang.getCXXAccessSpecifier(cursor) != CX_CXXAccessSpecifier.CX_CXXPublic && clang.getCXXAccessSpecifier(cursor) != CX_CXXAccessSpecifier.CX_CXXInvalidAccessSpecifier)
                        return CXChildVisitResult.CXChildVisit_Continue;
                    node = new NamedNodeVar(clang.getCursorSpelling(cursor).ToString(), clang.getTypeSpelling(clang.getCursorType(cursor)).ToString(), this.node);
                    if (clang.CXXField_isMutable(cursor) == 0)
                        ((NamedNodeVar)node).setImmutable();
                    break;
                case CXCursorKind.CXCursor_FunctionDecl:
                    if (clang.getCXXAccessSpecifier(cursor) != CX_CXXAccessSpecifier.CX_CXXPublic && clang.getCXXAccessSpecifier(cursor) != CX_CXXAccessSpecifier.CX_CXXInvalidAccessSpecifier)
                        return CXChildVisitResult.CXChildVisit_Continue;
                    node = new NamedNodeFunction(clang.getCursorSpelling(cursor).ToString(), clang.getCursorResultType(cursor).ToString(), null, this.node);
                    int funcParamCount = clang.Cursor_getNumArguments(cursor);
                    for (int i = 0; i < funcParamCount; i++)
                    {
                        CXCursor argument = clang.Cursor_getArgument(cursor, (uint)i);
                        String argType = clang.getTypeSpelling(clang.getCursorType(argument)).ToString();
                        String argName = clang.getCursorSpelling(argument).ToString();
                        ((NamedNodeFunction)node).AddParameter(argType, argName);
                    }
                    break;
                case CXCursorKind.CXCursor_StructDecl:
                    if (clang.getCXXAccessSpecifier(cursor) != CX_CXXAccessSpecifier.CX_CXXPublic && clang.getCXXAccessSpecifier(cursor) != CX_CXXAccessSpecifier.CX_CXXInvalidAccessSpecifier)
                        return CXChildVisitResult.CXChildVisit_Continue;
                    var structVisitor = new StructVisitor();
                    node = new NamedNodeStruct(clang.getCursorSpelling(cursor).ToString(), this.node);
                    visitor = new DmpVisitor(node);
                    clang.visitChildren(cursor, visitor.Visit, new CXClientData(IntPtr.Zero));
                    break;
                case CXCursorKind.CXCursor_EnumDecl:
                    if (clang.getCXXAccessSpecifier(cursor) != CX_CXXAccessSpecifier.CX_CXXPublic && clang.getCXXAccessSpecifier(cursor) != CX_CXXAccessSpecifier.CX_CXXInvalidAccessSpecifier)
                        return CXChildVisitResult.CXChildVisit_Continue;
                    var enumVisitor = new EnumVisitor();
                    clang.visitChildren(cursor, enumVisitor.Visit, new CXClientData(IntPtr.Zero));
                    string enumUnderlyingType = "";

                    switch(clang.getEnumDeclIntegerType(cursor).kind)
                    {
                        case CXTypeKind.CXType_Int: enumUnderlyingType = "int"; break;
                        case CXTypeKind.CXType_UInt: enumUnderlyingType = "uint"; break;
                        case CXTypeKind.CXType_Short: enumUnderlyingType = "short"; break;
                        case CXTypeKind.CXType_UShort: enumUnderlyingType = "ushort"; break;
                        case CXTypeKind.CXType_LongLong: enumUnderlyingType = "long"; break;
                        case CXTypeKind.CXType_ULongLong: enumUnderlyingType = "ulong"; break;
                        default:
                            Console.WriteLine("UNRECOGNIZED ENUM UNDERLYING TYPE: " + clang.getTypeKindSpelling(clang.getEnumDeclIntegerType(cursor).kind).ToString());
                            break;
                    }

                    var enumName = clang.getCursorSpelling(cursor).ToString();

                    if (string.IsNullOrEmpty(enumName))
                    {
                        var forwardDeclaringVisitor = new ForwardDeclaringVisitor(cursor);
                        clang.visitChildren(clang.getCursorLexicalParent(cursor), forwardDeclaringVisitor.Visit, new CXClientData(IntPtr.Zero));
                        enumName = clang.getCursorSpelling(forwardDeclaringVisitor.ForwardDeclarationCursor).ToString();  
                    }

                    if (string.IsNullOrEmpty(enumName))
                        enumName = "_";

                    node = new NamedNodeEnum(enumName, enumVisitor.Values, enumUnderlyingType, this.node);
                    break;
                case CXCursorKind.CXCursor_Constructor:
                    if (clang.getCXXAccessSpecifier(cursor) != CX_CXXAccessSpecifier.CX_CXXPublic)
                        return CXChildVisitResult.CXChildVisit_Continue;
                    node = new NamedNodeConstructor(clang.getCursorSpelling(cursor).ToString() + "_CREATE", null, this.node);
                    int conParamCount = clang.Cursor_getNumArguments(cursor);
                    for (int i = 0; i < conParamCount; i++)
                    {
                        CXCursor argument = clang.Cursor_getArgument(cursor, (uint)i);
                        String argType = clang.getTypeSpelling(clang.getCursorType(argument)).ToString();
                        String argName = clang.getCursorSpelling(argument).ToString();
                        ((NamedNodeConstructor)node).AddParameter(argType, argName);
                    }
                    break;
                case CXCursorKind.CXCursor_Destructor:
                    new NamedNodeDestructor(clang.getCursorSpelling(cursor).ToString() + "_DESTROY", this.node);
                    break;
            }

            return CXChildVisitResult.CXChildVisit_Continue;
        }
    }
}
