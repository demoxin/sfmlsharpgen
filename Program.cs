﻿using SFMLSharpGen.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace SFMLSharpGen
{
    class Program
    {
        public static HashSet<String> returnTypes = new HashSet<string>();
        public static HashSet<String> paramTypes = new HashSet<string>();

        static void Main(string[] args)
        {
            var libHeaderDir = Directory.GetCurrentDirectory() + "\\SFML\\include\\";
            var libGlobalClass = "Global";
            var genOutputDir = Directory.GetCurrentDirectory() + "\\GCSFML";

            List<string> parseFiles = new List<string>();

            string[] files = {
                "SFML\\System.hpp",
                "SFML\\Window.hpp",
                "SFML\\Audio.hpp",
                "SFML\\Graphics.hpp",
                "SFML\\Network.hpp"
            };

            foreach (var file in files)
                parseFiles.Add(libHeaderDir + "\\" + file);

            List<ClangSharp.CXTranslationUnit> translationUnits = new List<ClangSharp.CXTranslationUnit>();

            var createIndex = ClangSharp.clang.createIndex(0, 0);

            foreach(var header in parseFiles)
            {
                Console.WriteLine("Attempting to parse " + header + "...");
                ClangSharp.CXTranslationUnit translationUnit;
                ClangSharp.CXUnsavedFile unsavedFiles = new ClangSharp.CXUnsavedFile();

                var parseError = ClangSharp.clang.parseTranslationUnit2(createIndex, header, new string[] {"-I" + libHeaderDir, "-x c++" }, 1, out unsavedFiles, (uint)unsavedFiles.Length, 0, out translationUnit);

                if (parseError == ClangSharp.CXErrorCode.CXError_Success)
                {
                    Console.WriteLine("Parsed " + header + "!");
                }
                else
                {
                    Console.WriteLine("Error: " + parseError);
                    var numDiagnostics = ClangSharp.clang.getNumDiagnostics(translationUnit);

                    for (uint i = 0; i < numDiagnostics; ++i)
                    {
                        var diagnostic = ClangSharp.clang.getDiagnostic(translationUnit, i);
                        Console.WriteLine(ClangSharp.clang.getDiagnosticSpelling(diagnostic).ToString());
                        ClangSharp.clang.disposeDiagnostic(diagnostic);
                    }
                }

                translationUnits.Add(translationUnit);
            }

            var rootNode = new NamedNodeRoot("Root", null);
            rootNode.Output = false;
            var visitor = new DmpVisitor(rootNode);
            
            foreach (var tu in translationUnits)
                ClangSharp.clang.visitChildren(ClangSharp.clang.getTranslationUnitCursor(tu), visitor.Visit, new ClangSharp.CXClientData(IntPtr.Zero));

            Console.WriteLine("Beginning XML Dump...");
            var xmlSettings = new XmlWriterSettings();
            xmlSettings.Indent = true;
            var xml = XmlWriter.Create("dmp.xml", xmlSettings);
            xml.WriteStartDocument();
            rootNode.Write(xml);
            xml.WriteEndDocument();
            xml.Close();

            List<string> sortedReturnTypes = new List<string>(returnTypes);
            List<string> sortedParamTypes = new List<string>(paramTypes);
            sortedReturnTypes.Sort();
            sortedParamTypes.Sort();

            Console.WriteLine();
            Console.WriteLine("Known Return Types:");
            foreach (var returnType in sortedReturnTypes)
                Console.WriteLine("\t" + returnType);

            Console.WriteLine("Known Parameter Types:");
            foreach (var paramType in sortedParamTypes)
                Console.WriteLine("\t" + paramType);
            Console.WriteLine();

            Console.WriteLine("Finished.  Press any key to exit.");

            Console.ReadKey();
        }
    }
}
