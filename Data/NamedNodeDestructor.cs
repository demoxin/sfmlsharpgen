﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SFMLSharpGen.Data
{
    public class NamedNodeDestructor : NamedNode
    {
        public NamedNodeDestructor(string Name, NamedNode Parent) : base(Name, Parent)
        {
        }

        public override void Write(XmlWriter xml)
        {
            xml.WriteStartElement("Destructor");
            xml.WriteEndElement();
        }

        public override void WriteC(StreamWriter writer)
        {
            base.WriteC(writer);
        }

        public override void WriteCS(StreamWriter writer)
        {
            base.WriteCS(writer);
        }
    }
}
