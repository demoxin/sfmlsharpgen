﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SFMLSharpGen.Data
{
    public class NamedNodeEnum : NamedNode
    {
        private string UnderlyingType;
        private Dictionary<string, long> Values;

        public NamedNodeEnum(string Name, Dictionary<string, long> Values, string UnderlyingType, NamedNode Parent) : base(Name, Parent)
        {
            this.UnderlyingType = UnderlyingType;
            this.Values = Values;

            if (this.UnderlyingType.Equals(""))
                this.UnderlyingType = "int";

            if (this.Values == null)
                this.Values = new Dictionary<string, long>();
        }

        public override void Write(XmlWriter xml)
        {
            xml.WriteStartElement("Enum");
            xml.WriteAttributeString("Name", this.Name);
            xml.WriteAttributeString("UnderlyingType", this.UnderlyingType);
            
            if(Values != null && Values.Count > 0)
            {
                foreach(var value in Values)
                {
                    xml.WriteStartElement("Constant");
                    xml.WriteAttributeString("Value", value.Value.ToString());
                    xml.WriteString(value.Key);
                    xml.WriteEndElement();
                }
            }
            xml.WriteEndElement();
        }
    }
}
