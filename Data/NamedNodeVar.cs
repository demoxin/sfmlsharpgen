﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SFMLSharpGen.Data
{
    public class NamedNodeVar : NamedNode
    {
        private string nativeType;
        private bool isMutable;

        public NamedNodeVar(string Name, string NativeType, NamedNode Parent) : base(Name, Parent)
        {
            this.nativeType = NativeType;
            this.isMutable = true;
        }

        public void setImmutable()
        {
            this.isMutable = false;
        }

        public override void Write(XmlWriter xml)
        {
            var cMethodName = string.Join("_", this.GetParentNames());
            cMethodName += "_";

            var cMethodGetter = cMethodName + "get(void)";
            var cMethodSetter = cMethodName + "set(" + this.nativeType + ")";

            xml.WriteStartElement("Variable");
            xml.WriteAttributeString("Type", this.nativeType);
            xml.WriteAttributeString("Name", this.Name);
            xml.WriteStartElement("CFunction");
            xml.WriteStartElement("Get"); xml.WriteString(cMethodGetter.ToLower()); xml.WriteEndElement();
            if (isMutable)
            {
                xml.WriteStartElement("Set");
                xml.WriteString(cMethodSetter.ToLower());
                xml.WriteEndElement();
            }
            xml.WriteEndElement();
            xml.WriteEndElement();
        }
    }
}
