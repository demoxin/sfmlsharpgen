﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SFMLSharpGen.Data
{
    public class NamedNode
    {
        public string Name { get; private set; }
        public NamedNode Parent { get; private set; }
        public List<NamedNode> Children { get; private set; }

        public NamedNode GetNode(string Name)
        {
            if (Children.Count == 0)
                return new NamedNode(Name, this);

            foreach(var child in Children)
            {
                if (child.Name.Equals(Name))
                    return child;
            }

            return new NamedNode(Name, this);
        }

        public NamedNode(string Name, NamedNode Parent)
        {
            this.Name = Name;
            Children = new List<NamedNode>();

            if (Parent != null)
            {
                this.Parent = Parent;
                Parent.AddChild(this);
            }
        }

        public virtual string GetFullName()
        {
            if (Parent != null)
                return new string(string.Join("::", new string[] { Parent.GetFullName(), Name }).SkipWhile(chr => chr.Equals(':')).ToArray());
            return Name;
        }

        public virtual List<string> GetParentNames()
        {
            if(Parent == null)
                return new List<string>(new string[]{ this.Name });
            var ret = Parent.GetParentNames();
            ret.Add(this.Name);
            return ret;
        }

        public void AddChild(NamedNode node)
        {
            foreach (var child in Children)
                if (child.Name.Equals(node.Name))
                    return;
            Children.Add(node);
        }

        public virtual void Write(XmlWriter xml)
        {
            if (Children.Count == 0)
                return;
            xml.WriteStartElement("Namespace");
            xml.WriteAttributeString("Name", this.Name);
            foreach (var child in Children)
                child.Write(xml);
            xml.WriteEndElement();
        }

        public virtual void WriteC(StreamWriter writer)
        {
            foreach (var child in Children)
                WriteC(writer);
        }

        public virtual void WriteCS(StreamWriter writer)
        {
            writer.WriteLine("namespace " + Name);
            writer.WriteLine("{");
            foreach (var child in Children)
                WriteCS(writer);
            writer.WriteLine("}");
        }
    }
}
