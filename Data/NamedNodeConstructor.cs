﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SFMLSharpGen.Data
{
    public class NamedNodeConstructor : NamedNode
    {
        private Dictionary<string, string> Parameters;

        public NamedNodeConstructor(string Name, Dictionary<string, string> Parameters, NamedNode Parent) : base(Name, Parent)
        {
            this.Parameters = Parameters;

            if (this.Parameters == null)
                this.Parameters = new Dictionary<string, string>();
        }

        public void AddParameter(string type, string name)
        {
            Parameters.Add(name, type);
        }

        public override void Write(XmlWriter xml)
        {
            xml.WriteStartElement("Constructor");

            var cMethodName = string.Join("_", this.GetParentNames()) + "_CONSTRUCT";

            var cParams = "";

            Program.returnTypes.Add(string.Join("::", this.Parent.GetParentNames()));

            if (this.Parameters.Count > 0)
            {
                cMethodName += "_";

                var cParamList = new Dictionary<string, string>();
                foreach (var param in Parameters)
                {
                    Program.paramTypes.Add(param.Value);
                    cParamList.Add(param.Key, param.Value);
                }

                cParams = string.Join(", ", cParamList.Values);

                foreach (var param in Parameters)
                    cMethodName += param.Value.Split(':').Last().ToLower().Replace(" *", "ptr").Replace(" &", "ref") + "_";

                cMethodName = cMethodName.Substring(0, cMethodName.Length - 1);
            }
            else
            {
                cParams += "void";
            }

            cParams = "(" + cParams + ")";

            var cMethodSignature = cMethodName + cParams;

            foreach (var param in Parameters)
            {
                xml.WriteStartElement("Param");
                xml.WriteAttributeString("Type", param.Value);
                xml.WriteString(param.Key);
                xml.WriteEndElement();
            }

            xml.WriteEndElement();
        }

        public override void WriteC(StreamWriter writer)
        {
            base.WriteC(writer);
        }

        public override void WriteCS(StreamWriter writer)
        {
            base.WriteCS(writer);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is NamedNodeConstructor))
                return false;

            if (!((NamedNodeConstructor)obj).Name.Equals(this.Name))
                return false;

            if (this.Parameters.Equals(((NamedNodeConstructor)obj).Parameters))
                return false;

            return true;
        }
    }
}
