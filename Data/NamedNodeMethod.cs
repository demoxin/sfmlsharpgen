﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SFMLSharpGen.Data
{
    public class NamedNodeMethod : NamedNode
    {
        private string ReturnType;
        private Dictionary<string, string> Parameters;
        private bool isStatic;

        public NamedNodeMethod(string Name, string ReturnType, bool IsStatic, Dictionary<string, string> Parameters, NamedNode Parent) : base(Name, Parent)
        {
            this.ReturnType = ReturnType;
            this.Parameters = Parameters;
            this.isStatic = IsStatic;

            if (this.Parameters == null)
                this.Parameters = new Dictionary<string, string>();
        }

        public void AddParameter(string type, string name)
        {
            Parameters.Add(name, type);
        }

        public override void Write(XmlWriter xml)
        {
            if (isStatic)
                xml.WriteStartElement("StaticMethod");
            else
                xml.WriteStartElement("Method");
            xml.WriteAttributeString("Name", this.Name);
            xml.WriteAttributeString("ReturnType", this.ReturnType);

            var cMethodName = string.Join("_", this.GetParentNames());

            var cParams = "";

            Program.returnTypes.Add(this.ReturnType);

            if (this.Parameters.Count > 0)
            {
                cMethodName += "_";

                var cParamList = new Dictionary<string, string>();

                foreach (var param in Parameters)
                {
                    // TODO: Change library types to native types
                    Program.paramTypes.Add(param.Value);
                    cParamList.Add(param.Key, param.Value);
                }

                if (!isStatic)
                    cParams = this.Parent.Name + "*, ";

                cParams += string.Join(", ", cParamList.Values);

                foreach (var param in Parameters)
                    cMethodName += param.Value.Split(':').Last().ToLower().Replace(" *", "ptr").Replace(" &", "ref") + "_";

                cMethodName = cMethodName.Substring(0, cMethodName.Length - 1);
            }


            if(cParams == "")
            {
                cParams += "void";
            }

            cParams = "(" + cParams + ")";

            var cMethodSignature = cMethodName.ToLower() + cParams;

            xml.WriteStartElement("CFunction");
            xml.WriteString(cMethodSignature);
            xml.WriteEndElement();

            foreach (var param in Parameters)
            {
                xml.WriteStartElement("Param");
                xml.WriteAttributeString("Type", param.Value);
                xml.WriteString(param.Key);
                xml.WriteEndElement();
            }

            xml.WriteEndElement();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is NamedNodeMethod))
                return false;

            if (!((NamedNode)obj).Name.Equals(this.Name))
                return false;

            if (this.Parameters.Equals(((NamedNodeMethod)obj).Parameters))
                return false;

            return true;
        }
    }
}
