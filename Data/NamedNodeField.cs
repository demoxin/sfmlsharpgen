﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SFMLSharpGen.Data
{
    public class NamedNodeField : NamedNode
    {
        private string nativeType;
        private bool isStatic;

        public NamedNodeField(string Name, string NativeType, bool IsStatic, NamedNode Parent) : base(Name, Parent)
        {
            this.nativeType = NativeType;
            this.isStatic = IsStatic;
        }

        public override void Write(XmlWriter xml)
        {
            if (isStatic)
                xml.WriteStartElement("StaticField");
            else
                xml.WriteStartElement("Field");
            xml.WriteAttributeString("Type", this.nativeType);
            xml.WriteAttributeString("Name", this.Name);

            var cMethodName = string.Join("_", this.GetParentNames());
            cMethodName += "_";

            var cMethodGetter = cMethodName.ToLower() + "get(" + this.Parent.GetFullName() + "* self)";

            string cMethodSetter;
            if(!isStatic)
                cMethodSetter = cMethodName.ToLower() + "set(" + this.Parent.GetFullName() + "* self, " + this.nativeType + " " + this.Name +")";
            else
                cMethodSetter = cMethodName.ToLower() + "set(" + this.nativeType + " " + this.Name + ")";

            xml.WriteStartElement("CFunction");
            xml.WriteStartElement("Get");
            xml.WriteString(cMethodGetter);
            xml.WriteEndElement();
            xml.WriteStartElement("Set");
            xml.WriteString(cMethodSetter);
            xml.WriteEndElement();
            xml.WriteEndElement();

            xml.WriteEndElement();
        }
    }
}
