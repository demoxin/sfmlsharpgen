﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SFMLSharpGen.Data
{
    public class NamedNodeFunction : NamedNode
    {
        private string ReturnType;
        private Dictionary<string, string> Parameters;

        public NamedNodeFunction(string Name, string ReturnType, Dictionary<string, string> Parameters, NamedNode Parent) : base(Name, Parent)
        {
            this.ReturnType = ReturnType;
            this.Parameters = Parameters;

            if (this.Parameters == null)
                this.Parameters = new Dictionary<string, string>();
        }

        public void AddParameter(string type, string name)
        {
            Parameters.Add(name, type);
        }

        public override void Write(XmlWriter xml)
        {
            xml.WriteStartElement("Function");
            xml.WriteAttributeString("Name", this.Name);
            xml.WriteAttributeString("ReturnType", this.ReturnType);

            var cMethodName = string.Join("_", this.GetParentNames());

            var cParams = "";

            Program.returnTypes.Add(this.ReturnType);

            if (this.Parameters.Count > 0)
            {
                cMethodName += "_";

                var cParamList = new Dictionary<string, string>();
                foreach(var param in Parameters)
                {
                    // TODO: Convert types.
                    Program.paramTypes.Add(param.Value);
                    cParamList.Add(param.Key, param.Value);
                }

                cParams = string.Join(", ", cParamList.Values);

                foreach (var param in Parameters)
                    cMethodName += param.Value.Split(':').Last().ToLower().Replace(" *", "ptr").Replace(" &", "ref") + "_";

                cMethodName = cMethodName.Substring(0, cMethodName.Length - 1);
            }
            else
            {
                cParams += "void";
            }

            cParams = "(" + cParams + ")";

            var cMethodSignature = cMethodName + cParams;

            xml.WriteStartElement("CFunction");
            xml.WriteString(cMethodSignature);
            xml.WriteEndElement();

            if (Parameters != null && Parameters.Count > 0)
            {
                foreach (var param in Parameters)
                {
                    xml.WriteStartElement("Param");
                    xml.WriteAttributeString("Type", param.Value);
                    xml.WriteString(param.Key);
                    xml.WriteEndElement();
                }
            }
            xml.WriteEndElement();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is NamedNodeFunction))
                return false;

            if (!((NamedNode)obj).Name.Equals(this.Name))
                return false;

            if (this.Parameters.Equals(((NamedNodeFunction)obj).Parameters))
                return false;

            return true;
        }
    }
}
