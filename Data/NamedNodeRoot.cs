﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SFMLSharpGen.Data
{
    public class NamedNodeRoot : NamedNode
    {
        public bool Output = false;
        public NamedNodeRoot(string Name, NamedNode Parent) : base(Name, Parent)
        {

        }

        public override void Write(XmlWriter xml)
        {
            if (this.Output)
                xml.WriteStartElement("Root");

            foreach (var child in Children)
                child.Write(xml);

            if (this.Output)
                xml.WriteEndElement();
        }

        public override void WriteC(StreamWriter writer)
        {
            foreach (var child in Children)
                child.WriteC(writer);
        }

        public override void WriteCS(StreamWriter writer)
        {
            foreach (var child in Children)
                child.WriteCS(writer);
        }

        public override string GetFullName()
        {
            return "";
        }

        public override List<string> GetParentNames()
        {
            return new List<string>();
        }
    }
}
