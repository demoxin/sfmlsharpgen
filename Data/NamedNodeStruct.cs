﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SFMLSharpGen.Data
{
    public class NamedNodeStruct : NamedNode
    {
        public NamedNodeStruct(string Name, NamedNode Parent) : base(Name, Parent) { }

        public override void Write(XmlWriter xml)
        {
            if (Children.Count == 0)
                return;
            xml.WriteStartElement("Struct");
            xml.WriteAttributeString("Name", this.Name);
            foreach (var child in Children)
                child.Write(xml);
            xml.WriteEndElement();
        }

        public override void WriteCS(StreamWriter writer)
        {
            writer.WriteLine("struct " + this.Name);
            writer.WriteLine("{");
            foreach (var child in Children)
                child.WriteCS(writer);
            writer.WriteLine("}");
        }
    }
}
